#include <SoftwareSerial.h>
 
SoftwareSerial BTSerial(11,10); // RX | TX
String msg = "";

void setup()
{
  pinMode (11, INPUT);
  pinMode (10, OUTPUT);
  //pinMode(9, OUTPUT);  // this pin will pull the HC-05 pin 34 (key pin) HIGH to switch module to AT mode
  // digitalWrite(9, HIGH);
  Serial.begin(9600);
  Serial.println("BT connection : ok");
  BTSerial.begin(9600);  // HC-05 default speed in AT command more
}

void ecouterBT()
{
  while (BTSerial.available()) {
    // on a au moins 1 octet en attente
    int c = BTSerial.read(); // on lit la valeur. la fonction read retourne un entier: -1 en cas d'erreur sinon l'octet lu (dans l'octet de poids faible de l'entier)
    if (c != -1) { // s'il n'y a pas eu d'erreur de lecture
      //Serial.print(F("Octet lu: 0x")); Serial.print(c, HEX); // ici c est un entier, on affiche le code ASCII en Hexadécimal
      switch (c) {
        case '\n':
          Serial.print(F("message complet : ")); Serial.println(msg);
          msg = "";
          return;
        case '\r':
          break;
        default:
          msg += (char) c;
//          Serial.print(F(" caractere: [")); Serial.print((char) c); Serial.println(F("]"));
          break;
      }
    }
  }
}
  String msg2 = "123";
  
void loop()
{
  // Keep reading from HC-05 and send to Arduino Serial Monitor
  //ecouterBT();
  /*if (msg2.toInt() >=0 && msg2.toInt() <= 255){
    Serial.println("bonjour");
    msg2 = "256";
    msg = "123165465M";
    Serial.println(msg.toInt());
  }*/
  // Keep reading from Arduino Serial Monitor and send to HC-05
  /*if (Serial.available())
    BTSerial.write(Serial.read());*/
}
