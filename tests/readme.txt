testbluetooth : test de la commande BT (reception / emission)
testMedMoy : test de comparaison entre prendre la medianne et la moyenne des valeurs retournées par les capteurs
testMoteurs : tester le fonctionnement des moteurs
testSpeedSensor : tester le fonctionnement des capteurs de vitesse positionnés sur les moteurs
testUS : tester le fonctionnement des capteurs ultra son (en moyenne)
test2US : tester un autre fonctionnement des capteurs ultra son (en moyenne)
test_capteur_IR : tester le fonctionnement des capteurs infra rouge (en moyenne)