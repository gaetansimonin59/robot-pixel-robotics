const byte MOTORS_PINS[2][3] = { {22, 23, 2}, {24,25,3}};

void setSpeed(int motor, int motorspeed) {
  // Function that set the speed of an engine

  if (motorspeed > 0){ // Forward
    digitalWrite(MOTORS_PINS[motor][0], LOW); 
    digitalWrite(MOTORS_PINS[motor][1], HIGH);
  }
  else if (motorspeed < 0){ // Backward
    digitalWrite(MOTORS_PINS[motor][0], HIGH); 
    digitalWrite(MOTORS_PINS[motor][1], LOW);
  }
  else { 
    digitalWrite(MOTORS_PINS[motor][0], LOW); 
    digitalWrite(MOTORS_PINS[motor][1], LOW);
  }
  analogWrite(MOTORS_PINS[motor][2], abs(motorspeed));
}

void setup(){
  for (int i=0; i<2; i++){
    pinMode(MOTORS_PINS[i][0], OUTPUT);
    pinMode(MOTORS_PINS[i][1], OUTPUT);
    pinMode(MOTORS_PINS[i][2], OUTPUT);
  }
}

void loop(){
  setSpeed(0, 255);
  delay(2000);
  setSpeed(0, 50);
  delay(2000);
  setSpeed(0, 0);
  setSpeed(1, 255);
  delay(2000);
  setSpeed(1, 50);
  delay(2000);
  setSpeed(1, 0);
}
  
