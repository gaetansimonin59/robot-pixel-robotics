const int NB_CAPTEURS = 2;
const int NB_MESURES = 30;
float mesures[NB_CAPTEURS][50];
int done = 0;

/* Constantes pour les broches */
const byte TRIGGER_PIN[NB_CAPTEURS] = {23, 27}; // Broche TRIGGER
const byte ECHO_PIN[NB_CAPTEURS] = {22, 26};    // Broche ECHO

/* Constantes pour le timeout */
const unsigned long MEASURE_TIMEOUT = 25000UL; // 25ms = ~8m à 340m/s

/* Vitesse du son dans l'air en mm/µs */
const float SOUND_SPEED = 340.0 / 1000;

/** Fonction pour faire une aquisition avec les capteurs */
float aquisition(int capteur) {
  float somme = 0;
  for (int cpt = 0; cpt < NB_MESURES; cpt++) { /* 1. Lance une mesure de distance en envoyant une impulsion HIGH de 10µs sur la broche TRIGGER */
    digitalWrite(TRIGGER_PIN[capteur], HIGH);
    delayMicroseconds(10);
    digitalWrite(TRIGGER_PIN[capteur], LOW);
    
    /* 2. Mesure le temps entre l'envoi de l'impulsion ultrasonique et son écho (s'il existe) */
    long measure = pulseIn(ECHO_PIN[capteur], HIGH, MEASURE_TIMEOUT);

    /* 3. Calcul de la distance à partir du temps mesuré */
    float distance_mm = measure / 2.0 * SOUND_SPEED;

    /* 4. Calcul de la moyenne des 30 dernières mesures */
    somme = somme + distance_mm;
    delay(10);
  }
  return somme/NB_MESURES;
}



/** Fonction setup() */
void setup() {

  /* Initialise le port série */
  Serial.begin(9600);
   
  /* Initialise les broches */
  for (int i = 0; i < NB_CAPTEURS; i++) {
    pinMode(TRIGGER_PIN[i], OUTPUT);
    digitalWrite(TRIGGER_PIN[i], LOW); // La broche TRIGGER doit être à LOW au repos
    pinMode(ECHO_PIN[i], INPUT);
  }
}

void loop() {
  if (done == 0) {
    done = 1;
    for (int j = 0; j < 50; j++) {
      for (int capteur = 0; capteur < NB_CAPTEURS; capteur++) {
        mesures[capteur][j] = aquisition(capteur);
        delay(10);
      }
    }
    for (int i = 0; i < NB_CAPTEURS; i++) {
      for (int j = 0; j < 50; j++) {
        Serial.print(mesures[i][j]);
        Serial.print(",");
      }
      Serial.println("");
    }
  }
}
