int n = 20;
float tab[20];
float soundSpeed = 340.0/1000;

// Médiane d'un tableau trié
float mediane(){
    if (n % 2 == 0)
        return (tab[n / 2] + tab[n / 2 - 1]) / 2;
    else
        return tab[n / 2];
}

void fusion(int debut, int milieu, int fin){
        int n1 = milieu - debut + 1, n2 = fin - milieu, G[n1], D[n2];
     
        for (int i = 0; i < n1; i++) G[i] = tab[debut + i];
        for (int j = 0; j < n2; j++) D[j] = tab[milieu + 1 + j];
     
        // maintient trois pointeurs, un pour chacun des deux tableaux et un pour
        // maintenir l'index actuel du tableau trié final
        int i = 0, j = 0, k = debut;
     
        while (i < n1 && j < n2){
            if (G[i] <= D[j]){
                tab[k] = G[i];
                i++;
            }
            else{
                tab[k] = D[j];
                j++;
            }
            k++;
        }
     
        // Copiez tous les éléments restants du tableau non vide
        while (i < n1){
            tab[k] = G[i];
            i++;
            k++;
        }
     
        while (j < n2){
            tab[k] = D[j];
            j++;
            k++;
        }
    }
     
// Tri par fusion
void triFusion(int debut, int fin){
    if (debut < fin){
        // Trouvez le point milieu pour diviser le tableau en deux moitiés
        int m = (debut + fin) / 2;
 
        triFusion(debut, m);
        triFusion(m + 1, fin);
 
        // Fusionnez les deux moitiés triées
        fusion(debut, m, fin);
    }
}
 
float moyenne(){
    float sum = 0.;
    for (int i=0; i<n; i++){
        sum+=tab[i];
    }
    return sum/n;
}

float read_gp2d12_range(byte pin) {
  int tmp;
  tmp = analogRead(pin);
  if (tmp < 3)return -1;
  return (6787.0 /((float)tmp - 3.0)) - 4.0;
}

float read_us(byte pin1, byte pin2){
  digitalWrite(pin1, HIGH);
  delayMicroseconds(10);
  digitalWrite(pin1, LOW);
  return (float) pulseIn(pin2, HIGH, 25000UL)/2.0*soundSpeed;
}

void setup(){
  /*pinMode(22, OUTPUT);
  digitalWrite(22, LOW); // The TRIGGER pin must be at LOW at rest
  pinMode(23, INPUT);*/
  pinMode(A1, INPUT);
  Serial.begin(9600);
}

void loop(){
    for (int j=0; j<n; j++){
        for (int i=0; i<n; i++){
            //tab[i] = read_gp2d12_range(1)*10;
            tab[i] = read_us(22, 23);
        }

        triFusion(0, n-1);
        
        Serial.print("Serie : ");
        Serial.print(j);
        Serial.print(" Mediane : ");
        Serial.print(mediane());
        Serial.print(" Moyenne : ");
        Serial.println(moyenne());
    }
  delay(30000);
}
