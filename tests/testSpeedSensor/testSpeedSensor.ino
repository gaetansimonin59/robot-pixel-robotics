#define BAUDRATE 9600
#define NB_MOTORS 1
const byte SPEED_SENSORS_PINS[NB_MOTORS] = {2};
int barCounter[NB_MOTORS] = {0};      // Counter of the number of bar crossed

void barCounterM1(){ 
  barCounter[0]++; 
  Serial.println(barCounter[0]);
}

void setup(){
  Serial.begin(BAUDRATE);
  // Speed sensor setup
  for (int i=0; i<NB_MOTORS; i++) pinMode(SPEED_SENSORS_PINS[i], INPUT_PULLUP);
  attachInterrupt(0, barCounterM1, RISING);  //attachInterrupt(Port_nb, ISR, mode);
  cli();
}

void loop(){
  Serial.println("no interrupt");
  delay(5000);
  Serial.println("interrupt on");
  sei();
  while(1);
  
}

