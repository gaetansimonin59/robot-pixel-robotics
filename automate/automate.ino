/*
      automate.ino File : Contains setup and loop functions and all variables.

 Copyright (c) | 2021 | Pixel Robotics' project. All rights Reserved.
 Contributors : Titouan Azimzazdeh et Gaëtan Simonin.
 School : Polytech Lille.
 */

#include <avr/wdt.h>
#include <math.h>
#include <SoftwareSerial.h>

/* ------------Définition des constantes------------- */
#define MEASURE_TIMEOUT       3500UL                // timeout constant : 25ms = ~8m à 340m/s
#define SOUND_SPEED           340.0/1000             // Sound speed in the air [mm/us]
#define NB_US_SENSORS         4                      // number of ultrasonic sensors
#define NB_IR_SENSORS         7                      // number of infrared sensors
#define NB_MESURES            30                     // number of measures for an average distance
#define NB_MOTORS             4                      // Number of engines
#define PWM_RATIO             256                    // because in the formula : Vmax * 256/Vmax
#define NB_MAX_BAR            20                     // Maximum bars on the speed sensor wheel
#define ROBOT_DIAMETER        459.324                // Diametre du robot
#define WHEEL_DIAMETER        127                    // Length of the omnidirectional wheel in mm
#define DISTANCE_TRAVELED(m)  PI*WHEEL_DIAMETER*barCounter[m]/NB_MAX_BAR    // Conversion de la distance parcourue par une roue en fonction du nombre de barreaux
#define DEGRE_TO_RADIAN(a)    a*PI/180               // Fonction de conversion de degre en radian
#define RADIAN_TO_DEGRE(a)    a*180/PI                // Fonciton de conversion de radian en degre
#define txPin                 10                      // Port de communication sortant pour le module BT
#define rxPin                 11                      // Port de communication entratn pour le module BT
#define BAUDRATE              9600                    // baud rate
#define DISTANCE_BETWEEN_SIDE_SENSORS 135             // Distance entre 2 capteurs de distance sur le côté du robot
#define DISTANCE_BETWEEN_FRONT_SENSORS 100            // Distance entre 2 capteurs de distance à l'avant du robot

/* ------------------pins------------------ */
const byte US_SENSORS_PINS[NB_US_SENSORS][2] = {{30, 31}, {32, 33}, {34, 35}, {36, 37}};       // Array of Trigger and Echo pins of each ultrasonic distance sensor [trigger, echo]
const byte SPEED_SENSORS_PINS[NB_MOTORS] = {50, 51, 52, 53};                                   // Array of the pin map of every speed sensor : 50 front right, 51 rear right, 52 front left, 53 rear left
const byte IR_SENSORS_PINS[NB_IR_SENSORS] = {0, 1, 2, 3, 4, 5, 6};                             // Array of analog pin of each infrared sensor : 0 avant gauche, 1 avant droit, 2 gauche devant, 3 gauche derriere, 4 droit devant, 5 droite derriere, 6 arriere
const byte MOTORS_PINS[NB_MOTORS][3] = { {22, 23, 2}, {24, 25, 3}, {26, 27, 4}, {28, 29, 5}};  // [[Pin1, Pin2, SpeedPin], ...]

float mesures[NB_IR_SENSORS + NB_US_SENSORS] = {0};  // last measure of each sensor

int etape = 0;    //  0 : recherche de mur
                  //  1 : placement parallèle au mur
                  //  2 : recherche d'un angle
                  //  3 : calcul des points
                  //  4 : déplacement vers un point
                  //  5 : scan
bool arret = false;                   // true when robot have to stop
int capteur_arret;                    // capteur ayant demandé l'arret
int action;
int capteurs_mur[2];                  // capteurs face au mur
float mesures_mur[2];                 // mesures de ces capteurs lors d'un repositionnement face au mur
int sense_of_rotation;
int barCounter[4] = {0, 0, 0, 0};     // Counter of the number of bar crossed
int oldBarCounter[4] = {0};           // Save the old values of barCounter
int motion = 0, fatalError = 0;       // motion : 0=no move, 1=avant, 2=gauche, 3=avant-gauche, 4=avant-droite, 5=rotation clockwise (-=sens inverse)
float x = 0., y = 0., angle = 0.;     // robot coordinates
float xRef = x, yRef = y, angleRef = angle;
String msg;                           // message received by the BT Serial port
SoftwareSerial Bluetooth(rxPin ,txPin); // Bluetooth Serial Port
bool testingEndOfWall = false;
int mur = 0;
int origines[30][2];
int tmpOrigin[3] = {0, 0, 0};
bool obstacle = false, backward = false;
int obDetect[NB_IR_SENSORS + NB_US_SENSORS] = {0};
int potar;

typedef struct {
  int intensity;  // in percentage
  float angle;  // in degre
} Vector;

// Fonction setup()
void setup() {

  //origine du premier mur
  origines[0][0] = 0;
  origines[0][1] = 0;

  // Distance sensors setup
  // IR sensors
  for (int i=NB_IR_SENSORS; i<NB_IR_SENSORS; i++){
    pinMode(IR_SENSORS_PINS[i], INPUT);
  }
  // US sensors
  for (int i=NB_IR_SENSORS; i<NB_US_SENSORS; i++){
    pinMode(US_SENSORS_PINS[i][0], OUTPUT);
    digitalWrite(US_SENSORS_PINS[i][0], LOW); // The TRIGGER pin must be at LOW at rest
    pinMode(US_SENSORS_PINS[i][1], INPUT);
  }

  // Motor setup
  for (int i=0; i<NB_MOTORS; i++){
    pinMode(MOTORS_PINS[i][0], OUTPUT);
    pinMode(MOTORS_PINS[i][1], OUTPUT);
    pinMode(MOTORS_PINS[i][2], OUTPUT);
  }

  // Speed sensor setup
  for (int i=0; i<NB_MOTORS; i++) pinMode(SPEED_SENSORS_PINS[i], INPUT_PULLUP);

  // Communication with the scanner via the following digital port

  // Interruptions setup
  // PD0 -> PD3 : broches 25 à 28
  attachInterrupt(0, barCounterM1, RISING);  //attachInterrupt(Port_nb, ISR, mode);
  attachInterrupt(1, barCounterM2, RISING);
  attachInterrupt(2, barCounterM3, RISING);
  attachInterrupt(3, barCounterM4, RISING);
  WDTCSR = (1 << WDCE) | (1 << WDE);
  WDTCSR = (1<<WDIE)| (1<<WDP1);
  cli();  // Disable interruptions

  // Serial Port setup
  Serial.begin(BAUDRATE);
}


ISR(WDT_vect) {
  // interruption that occurs on a regular basis
  bool hasMoved = false;
  for (int i=0; i<NB_MOTORS; i++){
    if (oldBarCounter[i] != barCounter[i]){
      hasMoved = true;
    }
  }
  if (hasMoved){
    whereAmI();
    oldBarCounter[0] = barCounter[0];
    oldBarCounter[1] = barCounter[1];
    oldBarCounter[2] = barCounter[2];
    oldBarCounter[3] = barCounter[3];
  }
}

void Scanner(){
  // Launch a 3D scan
  Serial.println("scan en cours");
}

void loop() {
  aquisitionIR(mesures, 0, NB_IR_SENSORS-1);
  aquisitionUS(&(mesures[NB_IR_SENSORS]));
  /* Vérification de la distance de sécurité */
  int dist_max = 250;
  for (int i = 0; i < NB_IR_SENSORS+NB_US_SENSORS; i++) {
    if (i == NB_IR_SENSORS) dist_max = 1500;
    if (mesures[i] < dist_max) { // il faut arreter le robot dans ce cas-là
      stopMoving();
      arret = true;
      capteur_arret = i;
      break;
    }
  }

  /* Gestion du robot en fonction de ce qu'il doit faire */
  switch(etape) {

    case 0 : // le robot avance jusqu'à 30cm d'un obstacle (considéré comme un mur, l'opérateur doit avoir placé le robot en conséquence)
      if (!arret) {
        trFrontBack(50, 1); // avancer
        break;
      } else {
        arret = false;
        if (capteur_arret == 0 || capteur_arret == 2 || capteur_arret == 3) {
          // passage à l'étape 1 avec rotation à droite
          capteurs_mur[0] = 2;
          capteurs_mur[1] = 3;
          etape = 1;
          sense_of_rotation = -1;
        }
        else if (capteur_arret == 1 || capteur_arret == 4 || capteur_arret == 5) {
          // passage à l'étape 1 avec rotation à gauche
          capteurs_mur[0] = 4;
          capteurs_mur[1] = 5;
          etape = 1;
          sense_of_rotation = 1;
        }
        else {
          trFrontBack(50, 1); // avancer
          break;
        }
      }

    case 1 : // se placer parallèle au mur
      arret = false;
      while (1) {
        aquisitionIR(mesures_mur, capteurs_mur[0], capteurs_mur[1]);
        if (mesures_mur[0] > 295 and mesures_mur[0] < 305 and mesures_mur[1] > 295 and mesures_mur[1] < 305) {
          // le robot est parallèle, passage à l'étape 2
          stopMoving();
          etape = 2;
          break;
        }
        else {
          if (mesures_mur[0] < 295 and mesures_mur[1] < 295) {
            // les deux capteurs sont trop près, il faut s'éloigner du mur
            action = 0;
          }
          else if (mesures_mur[0] > 305 and mesures_mur[1] > 305) {
            // les deux capteurs sont trop loin, il faut se rapprocher du mur
            action = 1;
          }
          else if (mesures_mur[0] < mesures_mur[1]) {
            // tourner dans le bon sens
            action = 2;
          }
          else {
            // tourner dans l'autre sens
            action = 3;
          }
          switch (action) {
          case 0 :
            if (sense_of_rotation == -1) {
              trLeftRight(50, -1); // aller à droite
            }
            else {
              trLeftRight(50, 1); // aller à gauche
            }
            break;

          case 1 :
            if (sense_of_rotation == -1) {
              trLeftRight(50, 1); // aller à gauche
            }
            else {
              trLeftRight(50, -1); // aller à droite
            }
            break;

          case 2 :
            if (sense_of_rotation == -1) {
              rotation(50, 1); //tourner
            }
            else {
              rotation(50, -1);// tourner
            }
            break;

          case 3 :
            if (sense_of_rotation == -1) {
              rotation(50, -1); //tourner
            }
            else {
              rotation(50, 1); // tourner
            }
            break;
          }
        }
      }
      break;

    case 2 : // se déplacer jusqu'à l'angle suivant
      if (arret == false) {
        // vérifier qu'on ne s'éloigne pas du mur ou qu'on arrive pas à un angle ouvert
        if (mesures[capteurs_mur[0]] > 305 and 295 < mesures[capteurs_mur[1]] < 305) { // on s'éloigne du mur ou on arrive au mur suivant
          // stratégie : laisser avancer un peu pour vérifier si c'est bien une fin de mur : si le capteur arrière reste à la meme distance du mur, c'est un nouveau, sinon c'est juste qu'on est pas aligné
          if (!testingEndOfWall) {
            testingEndOfWall = true;
            tmpOrigin[0] = 0;
            tmpOrigin[1] = 0;
            tmpOrigin[2] = 0;
            trFrontBack(50, 1);
          } else {
            if (mesures[capteurs_mur[1]] > 305) {
              testingEndOfWall = false;
              etape = 1;
            } else {
              if (sq(x-tmpOrigin[0])+sq(y-tmpOrigin[1]) >= sq(DISTANCE_BETWEEN_SIDE_SENSORS)) {
                stopMoving();
                // placer l'origine du mur 135/2 mm derrière la position actuelle du robot
                if (mur == 0) { // on replace l'origine
                  x = DISTANCE_BETWEEN_SIDE_SENSORS;
                  y = 0;
                  angle = 0;
                }
                mur++;
                //SE PLACER PRES DU MUR SUIVANT (à ajouter)
              }
            }
          }
        } else if (295 < mesures[capteurs_mur[0]] < 305 and 295 < mesures[capteurs_mur[1]] < 305) {
          trFrontBack(100, 1);
        } else {
          etape == 1; // le robot doit se replacer par rapport au mur
        }
      } else {
        if (capteur_arret == 0) {
          if (mesures[0]-5 < mesures[1] < mesures[0]+5) { // le prochain mur est à angle droit avec celui là
            origines[mur][0] = x + mesures[0]*cos(angle) - sense_of_rotation*mesures[capteurs_mur[1]]*sin(angle);
            origines[mur][1] = y + mesures[0]*sin(angle) - sense_of_rotation*mesures[capteurs_mur[1]]*cos(angle);
            origines[mur][2] = angle + sense_of_rotation*90;
            rotateOf(sense_of_rotation*90);
            mur++;
            etape = 1;
          } else if (mesures[1] > 700) {
            // le robot détecte un mur avec un angle très faible ou juste un obstacle moins large que lui
          } else {
            float a = DISTANCE_BETWEEN_FRONT_SENSORS/(mesures[0]-mesures[1]);
            float b = DISTANCE_BETWEEN_FRONT_SENSORS/2 - a * (mesures[0]+200);
            float yp = -sense_of_rotation*(200+mesures[capteurs_mur[0]]);
            float xp = (yp-b)/a;
            origines[mur][0] = x + xp*cos(angle) + yp*sin(angle);
            origines[mur][1] = y + xp*sin(angle) + yp*cos(angle);
            origines[mur][2] = angle + asin(DISTANCE_BETWEEN_FRONT_SENSORS/(mesures[0]-mesures[1]));
            rotateOf(asin(DISTANCE_BETWEEN_FRONT_SENSORS/(mesures[1]-mesures[0])));
            mur++;
            etape = 1;
          }
        } else if (capteur_arret == 1) {
          if (mesures[1]-5 < mesures[0] < mesures[1]+5) { // le prochain mur est à angle droit avec celui là
            origines[mur][0] = x + mesures[0]*cos(angle) - sense_of_rotation*mesures[capteurs_mur[1]]*sin(angle);
            origines[mur][1] = y + mesures[0]*sin(angle) - sense_of_rotation*mesures[capteurs_mur[1]]*cos(angle);
            origines[mur][2] = angle + sense_of_rotation*90;
            rotateOf(sense_of_rotation*90);
            mur++;
            etape = 1;
          } else if (mesures[0] > 700) {
            // le robot détecte un mur avec un angle très faible ou juste un obstacle moins large que lui
          } else {
            float a = DISTANCE_BETWEEN_FRONT_SENSORS/(mesures[0]-mesures[1]);
            float b = DISTANCE_BETWEEN_FRONT_SENSORS/2 - a * (mesures[0]+200);
            float yp = -sense_of_rotation*(200+mesures[capteurs_mur[0]]);
            float xp = (yp-b)/a;
            origines[mur][0] = x + xp*cos(angle) + yp*sin(angle);
            origines[mur][1] = y + xp*sin(angle) + yp*cos(angle);
            origines[mur][2] = angle + asin(DISTANCE_BETWEEN_FRONT_SENSORS/(mesures[0]-mesures[1]));
            rotateOf(-asin(DISTANCE_BETWEEN_FRONT_SENSORS/(mesures[0]-mesures[1])));
            mur++;
            etape = 1;
          }
        } else if (capteur_arret == 6) {
          trFrontBack(100, 1); // le robot avance s'il est trop près du mur derrière lui
        }
      }
      break;
    }
  }
}


/* A AJOUTER

Déplacer le robot vers le mur suivant après détection d'un angle rentrant
A chaque détection d'angle, mettre une condition pour quand c'est le premier angle
A chaque détection d'angle, vérifier qu'on est pas revenu au premier
Calcul des longueurs et angles des murs à partir des angles
Traduire scanPiece.py en C/C++ pour l'ajouter ici
Ajouter le comportement du robot aux étapes 4 et 5 (déplacement vers les points et scan, des fonctions sont déjà faites pour cela)

*/
