/*
      bluetooth.ino File : Contains all functions for Bluetooth communication.

 Copyright (c) | 2021 | Pixel Robotics' project. All rights Reserved.
 Contributors : Titouan Azimzazdeh et Gaëtan Simonin.
 School : Polytech Lille.
 */

void readBTSerialPort(){
  // bluetooth module function
  msg="";
  while (Bluetooth.available()) {
    delay(10);
    if (Bluetooth.available() > 0) {
      char c = Bluetooth.read();  //gets one byte from serial buffer
      msg += c; //makes the string readString
    }
  }
}

void convertionCmdBT(String msg){
	// potar de vitesse à prendre en compte

	switch (msg){
		case "0": // stop
			stopMoving();
			motion = 0;
			arret = 1;
			break;
		case "1": // front
			motion = 1;
			arret = 0;
			trFrontBack(potar, 1);
			break;
		case "2": // back
			motion = -1;
			arret = 0;
			trFrontBack(potar, -1);
			break;
		case "3": // left
			motion = 2;
			arret = 0;
			trLeftRight(potar, 1);
			break;
		case "4": // right
			motion = -2;
			arret = 0;
			trLeftRight(potar, -1);
			break;
		case "5": // front left
			motion = 3;
			arret = 0;
			diag(potar, 1, 1);
			break;
		case "6": // back right
			motion = -3;
			arret = 0;
			diag(potar, -1, 0);
			break;
		case "7": // front right
			motion = 4;
			arret = 0;
			diag(potar, 1, 0);
			break;
		case "8": // back left
			motion = -4;
			arret = 0;
			diag(potar, -1, 1);
			break;
		case "9": // rotation left
			motion = 5;
			arret = 0;
			rotation(potar, -1);
			break;
		case "10": // rotation right
			motion = -5;
			arret = 0;
			rotation(potar, 1);
			break;
		case "11": // run
			arret = 0;
			break;
		case "12": // reset
			stopMoving();
			arret = 1;
			motion = 0;
			etape = 0;
			break;
		default:
			fatalError = 1;
			break;
	}
	resetSpeedSensors();
	whereAmI();
}
