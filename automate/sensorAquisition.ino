/*
      sensorAquisition.ino File : Contains all functions to aquire sensors' measures.

 Copyright (c) | 2021 | Pixel Robotics' project. All rights Reserved.
 Contributors : Titouan Azimzazdeh et Gaëtan Simonin.
 School : Polytech Lille.
 */

float read_gp2d12_range(byte pin) {
  // fonction donnant la distance entre le capteur "pin" et l'objet
  int tmp;
  tmp = analogRead(pin);
  if (tmp < 3) return -1;
  return (6787.0 /((float)tmp - 3.0)) - 4.0;
}

/** Fonction pour faire une aquisition avec les capteurs infrarouges */
void aquisitionIR(float* mesures, int capteur_debut, int capteur_fin) {
  for (int capteur = capteur_debut; capteur <= capteur_fin; capteur++) {
    float measures[NB_MESURES];
    for (int cpt = 0; cpt < NB_MESURES; cpt++) {
      float distance_mm = read_gp2d12_range(IR_SENSORS_PINS[capteur])*10;
      measures[cpt] = distance_mm;
    }
    mesures[capteur] = mediane(measures, NB_MESURES);
  }
}

/** Fonction pour faire une aquisition avec les capteurs ultrason */
void aquisitionUS(float* mesures) {
  for (int capteur = 0; capteur < NB_US_SENSORS; capteur++) {
    float measures[NB_MESURES];
    for (int cpt = 0; cpt < NB_MESURES; cpt++) {
      digitalWrite(US_SENSORS_PINS[capteur][0], HIGH);
      delayMicroseconds(10);
      digitalWrite(US_SENSORS_PINS[capteur][0], LOW);
      long measure = pulseIn(US_SENSORS_PINS[capteur][1], HIGH, MEASURE_TIMEOUT);
      float distance_mm = measure / 2.0 * SOUND_SPEED;
      measures[cpt] = distance_mm;
    }
    mesures[capteur] = mediane(measures, NB_MESURES);
  }
}

// Médiane d'un tableau trié
float mediane(float* tab, int length) {
  triFusion(0, length-1, tab);
  if (length % 2 == 0) return (tab[length / 2] + tab[length / 2 - 1]) / 2;
  else return tab[length / 2];
}

void fusion(int debut, int milieu, int fin, float* tab){
  int n1 = milieu - debut + 1, n2 = fin - milieu, G[n1], D[n2];

  for (int i = 0; i < n1; i++) G[i] = tab[debut + i];
  for (int j = 0; j < n2; j++) D[j] = tab[milieu + 1 + j];

  // maintient trois pointeurs, un pour chacun des deux tableaux et un pour
  // maintenir l'index actuel du tableau trié final
  int i = 0, j = 0, k = debut;

  while (i < n1 && j < n2) {
    if (G[i] <= D[j]){
      tab[k] = G[i];
      i++;
    } else {
      tab[k] = D[j];
      j++;
    }
    k++;
  }

  // Copiez tous les éléments restants du tableau non vide
  while (i < n1) {
    tab[k] = G[i];
    i++;
    k++;
  }

  while (j < n2) {
    tab[k] = D[j];
    j++;
    k++;
  }
}

// Tri par fusion
void triFusion(int debut, int fin, float* tab) {
  if (debut < fin) {
    // Trouvez le point milieu pour diviser le tableau en deux moitiés
    int m = (debut + fin) / 2;

    triFusion(debut, m, tab);
    triFusion(m + 1, fin, tab);

    // Fusionnez les deux moitiés triées
    fusion(debut, m, fin, tab);
  }
}
