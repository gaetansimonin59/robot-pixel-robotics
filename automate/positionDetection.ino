/*
      positionDetection.ino File : Contains all functions to detect the position of the robot.

 Copyright (c) | 2021 | Pixel Robotics' project. All rights Reserved.
 Contributors : Titouan Azimzazdeh et Gaëtan Simonin.
 School : Polytech Lille.
 */

void barCounterM1() {
  barCounter[0]++;
}

void barCounterM2() {
  barCounter[1]++;
}

void barCounterM3() {
  barCounter[2]++;
}

void barCounterM4() {
  barCounter[3]++;
}

void resetSpeedSensors(){
  for (int i=0; i<4; i++){
    barCounter[i] = 0;
  }
}

int similarDist2(float a, float b){
  float highRef = 1.05*a, lowRef = 0.95*a;
  if (b >= lowRef && b <= highRef){
    return 1;
  }
  return 0;
}

int similarDist4(float a, float b, float c, float d){
  float highRef = 1.05*a, lowRef = 0.95*a;
  if (b >= lowRef && b <= highRef && c >= lowRef && c <= highRef && d >= lowRef && d <= highRef){
        return 1;
  }
  return 0;
}

void whereAmI(){
  // Get the traveled distance from the last movement type

  float d1 = DISTANCE_TRAVELED(0);
  float d2 = DISTANCE_TRAVELED(1);
  float d3 = DISTANCE_TRAVELED(2);
  float d4 = DISTANCE_TRAVELED(3);

  if (motion == 1 || motion == -1){ // Front / Back
    if (similarDist4(d1, d2, d3, d4) == 1){
      float moy = (d1+d2+d3+d4)/4;
      x += motion*moy*cos(DEGRE_TO_RADIAN(angle)); // angle : positif ou negatif
      y += motion*moy*sin(DEGRE_TO_RADIAN(angle));
    }
    else fatalError = 1;
  }
  else if (motion == 2 || motion == -2){ // Left / Right
    if (similarDist4(d1, d2, d3, d4) == 1){
      int coeff = motion/2;
      float moy = (d1+d2+d3+d4)/4;
      x += coeff*moy*cos(DEGRE_TO_RADIAN(angle) + PI/2);
      y += coeff*moy*sin(DEGRE_TO_RADIAN(angle) + PI/2);
    }
    else fatalError = 1;
  }
  else if (motion == 3 || motion == -3){ // Diag Front Left / Back Right
    if (similarDist2(d2, d4) == 1){
      int coeff = motion/3;
      float moy = (d1+d2+d3+d4)/4;
      x += coeff*moy*cos(DEGRE_TO_RADIAN(angle) + PI/4);
      y += coeff*moy*sin(DEGRE_TO_RADIAN(angle) + PI/4);
    }
    else fatalError = 1;
  }
  else if (motion == 4 || motion == -4){ // Diag Front Right / Back Left
    if (similarDist2(d1, d3) == 1){
      int coeff = motion/4;
      float moy = (d1+d2+d3+d4)/4;
      x += coeff*moy*cos(DEGRE_TO_RADIAN(angle) - PI/4);
      y += coeff*moy*sin(DEGRE_TO_RADIAN(angle) - PI/4);
    }
    else fatalError = 1;
  }
  else if (motion == 5 || motion == -5){ // Rotation Left / Right
    // x, y, unchanged
    if (similarDist4(d1, d2, d3, d4) == 1){
      int coeff = motion/3;
      float moy = (d1+d2+d3+d4)/4;
      angle += coeff*moy*RADIAN_TO_DEGRE(2/ROBOT_DIAMETER);
    }
    else fatalError = 1;
  }
  // motion == 0 : stop => x, y unchanged
  return;
}
