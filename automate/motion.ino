/*
      motion.ino File : Contains all motion functions.

 Copyright (c) | 2021 | Pixel Robotics' project. All rights Reserved.
 Contributors : Titouan Azimzazdeh et Gaëtan Simonin.
 School : Polytech Lille.
 */

void setSpeed(int motor, int speed) {
  // Function that set the speed of an engine

  if (speed > 0){ // Forward
    digitalWrite(MOTORS_PINS[motor][0], LOW);
    digitalWrite(MOTORS_PINS[motor][1], HIGH);
  }
  else if (speed < 0){ // Backward
    digitalWrite(MOTORS_PINS[motor][0], HIGH);
    digitalWrite(MOTORS_PINS[motor][1], LOW);
  }
  else {
    digitalWrite(MOTORS_PINS[motor][0], LOW);
    digitalWrite(MOTORS_PINS[motor][1], LOW);
  }
  analogWrite(MOTORS_PINS[motor][2], abs(speed));
}

void rotation(int speed, int sense){
  /*  Rotation function
   speed in pourcentage
   sense = 1 : clockwise rotation
   sense = -1 : anti-clock rotation
   */
    motion = (-1)*sense*5;
    int V = sense * floor((speed/100) * PWM_RATIO + 0.5); // Speed between 0 and 255
    setSpeed(0, V);
    setSpeed(1, (-1)*V);
    setSpeed(2, (-1)*V);
    setSpeed(3, V);
}

void trFrontBack(int speed, int sense){
  /*  Forward - Backward translation function
   speed in pourcentage
   sense = 1 : Forward translation
   sense = -1 : Backward translation
   */
    motion = sense;
    int V = sense * floor((speed/100) * PWM_RATIO + 0.5); // Speed between 0 and 255
    for (int i=0; i<NB_MOTORS; i++) setSpeed(i, V);
}

void trLeftRight(int speed, int sense){
  /*  Left - Right translation function
   speed in pourcentage
   sense = 1 : Left translation
   sense = -1 : Right translation
   */
    motion = sense*2;
    int V = sense * floor((speed/100) * PWM_RATIO + 0.5); // Speed between 0 and 255
    setSpeed(0, V);
    setSpeed(1, (-1)*V);
    setSpeed(2, V);
    setSpeed(3, (-1)*V);
}

void trDiagonal(int speed, int sense, int diagonal){
  /*  Diagonal translations function
   speed in pourcentage

   sense = 1 : Forward translation
   sense = -1 : Backward translation

   diagonal = 0 : Right forward direction or left backward direction
   diagonal = 1 : Left forward direction or right backward direction
   */
    int V = sense * floor((speed/100) * PWM_RATIO + 0.5); // Speed between 0 and 255
    if (diagonal == 0){
      motion = sense*4;
      setSpeed(0, V);
      setSpeed(1, 0);
      setSpeed(2, V);
      setSpeed(3, 0);
    }
    else {
      motion=sense*3;
      setSpeed(0, 0);
      setSpeed(1, V);
      setSpeed(2, 0);
      setSpeed(3, V);
    }
}

void stopMoving(){
  motion = 0;
  for (int i=0; i<NB_MOTORS; i++) setSpeed(i, 0);
}

/*
void commandLaw(Vector robotSpeed) {
 // Command law function which determines the speed values to distribute to engines

 float speedForward = robotSpeed.intensity*cos(robotSpeed.angle);
 float speedRight = robotSpeed.intensity*sin(robotSpeed.angle);

 int V1, V2;
 V1 = floor(((speedForward + speedRight)/100) * PWM_RATIO + 0.5); // Motor 1 and 3 have the same speed
 V2 = floor(((speedForward - speedRight)/100) * PWM_RATIO + 0.5); // Motor 2 and 4 have the same speed

 setSpeed(0, V1);
 setSpeed(1, V2);
 setSpeed(2, V1);
 setSpeed(3, V2);
 }
 */

int distanceToSpeed(float remainingDistance){
  // Return the robot speed
  // distanceToGo and distanceTraveled in mm

  if (remainingDistance < 1){
    return 0;
  }
  if (remainingDistance < 1000){ // Last 50 cm
    return (int) (remainingDistance/10); // linear deceleration
  }
  float x_diff = x-xRef;
  float y_diff = y-yRef;
  float distanceTraveled = sqrt(x_diff*x_diff + y_diff*y_diff);
  if (distanceTraveled < 1000) { // First 50 cm
    return (int) (distanceTraveled/10); // linear acceleration
  }
  return 100;
}

int obstacleToSpeed(float distance){
  if (distance <= 200){
    return 0;
  }
  if (distance >= 500){
    return 100;
  }
  return (int) (0.2*distance-25);
}

void setCoordRef(){
  angleRef = angle;
  xRef = x;
  yRef = y;
}

void rotateOf(float angleToReach){
  // Tourner de x degre avec les barCounter
  float ecart = angle-angleToReach;
  while (angle != angleToReach){
    if (ecart <= 0 && ecart >= -180){
      rotation(50, -1);
    }
    else{
      rotation(50, 1); // rotation jusqu'a atteindre l'angle souhaité
    }
  }
  stopMoving();
}

// Attention a bien faire appel a la fonction setCoordRef();
void goTo(float x_dest, float y_dest){
  if (!obstacle){
    // y = ax+b
    float x_diff = x_dest-x;
    float y_diff = y_dest-y;
    //float a = y_diff / x_diff;
    //float b = y_dest-a*x_dest;

    float norme = sqrt(x_diff*x_diff + y_diff*y_diff);
    rotateOf(RADIAN_TO_DEGRE(atan(y_diff / norme)));
    trFrontBack(distanceToSpeed(norme), 1);
  }
}

bool obstacleDetected(int sensor){
  if (mesures[sensor] >= 200 && mesures[sensor] <= 500) {
    return true;
  }
  return false;
}

int getTabObstacleDetected(){
  int cpt = 0;
  for (int i=0; i<(NB_IR_SENSORS+NB_US_SENSORS); i++){
    if (obstacleDetected(i)){
      obDetect[cpt] = i;
      cpt++;
    }
  }
  return cpt;
}

float closestObstacle(){
  float measure = 500.;
  for (int i=0; i<=(NB_IR_SENSORS+NB_US_SENSORS); i++){
    if (measure > mesures[i]){
      measure = mesures[i];
    }
  }
  return measure;
}

void obstacleDetection(int nbDetected){
  // gestion des Obstacles entre 20 et 50 cm
  if (!obstacle){   // blocage lors de la présence d'un obstacle
                    // Utilise les commandes de cette fonction et non pas celles de goTo
    // obstacle devant
    if (obstacleDetected(0) && obstacleDetected(1)){
      obstacle = true;
      // gauche libre
      if (!obstacleDetected(2) && !obstacleDetected(3) && mesures[0] >= mesures[1]){
        while (obstacleDetected(1)){
          trLeftRight(obstacleToSpeed(mesures[1]), 1);
        }
      }
      // droite libre
      else if (!obstacleDetected(4) && !obstacleDetected(5) && mesures[1] >= mesures[0]){
        while (obstacleDetected(0)){
          trLeftRight(obstacleToSpeed(mesures[0]), -1);
        }
      }
      /*// gauche pas libre
      else if ((obstacleDetected(2) || obstacleDetected(3)) && !obstacleDetected(4) && !obstacleDetected(5)){
        while(obstacleDetected(0)){
          trLeftRight(obstacleToSpeed(mesures[1]), -1);
        }
      }*/
      // devant + gauche + droite : bloqué
      // arrière libre
      else if (obstacleDetected(0) && obstacleDetected(1) && obstacleDetected(2) && obstacleDetected(3) && obstacleDetected(4) && obstacleDetected(5)){
        backward = 1;
        while(obstacleDetected(0) || obstacleDetected(1) || obstacleDetected(2) || obstacleDetected(3) || obstacleDetected(4) || obstacleDetected(5)){
          trFrontBack(obstacleToSpeed(closestObstacle()), -1);
        }
      }
      // Tout autre cas
      else {
        fatalError = 1;
        arret = 1;
        motion = 0;
        stopMoving();
      }
    }
    // obstacle à gauche
    else if (obstacleDetected(2) && obstacleDetected(3)){
      obstacle = true;
      // devant libre
      if (!obstacleDetected(0) && !obstacleDetected(1) && mesures[2] >= mesures[3]){
        while (obstacleDetected(3)){
          trFrontBack(obstacleToSpeed(mesures[3]), 1);
        }
      }
      // derrière libre
      else if (!obstacleDetected(6) && mesures[3] >= mesures[2]){
        backward = 1;
        while (obstacleDetected(2)){
          trFrontBack(obstacleToSpeed(mesures[2]), -1);
        }
      }
      // devant + gauche + derrière : bloqué
      // droite libre
      else if (obstacleDetected(0) && obstacleDetected(1) && obstacleDetected(2) && obstacleDetected(3) && obstacleDetected(6)){
        backward = 1;
        while(obstacleDetected(0) || obstacleDetected(1) || obstacleDetected(2) || obstacleDetected(3) || obstacleDetected(6)){
          trLeftRight(obstacleToSpeed(closestObstacle()), -1);
        }
      }
      // Tout autre cas
      else {
        fatalError = 1;
        arret = 1;
        stopMoving();
      }
    }
    // obstacle à droite
    else if (obstacleDetected(4) && obstacleDetected(5)){
      obstacle = true;
      // devant libre
      if (!obstacleDetected(0) && !obstacleDetected(1) && mesures[4] >= mesures[5]){
        while (obstacleDetected(5)){
          trFrontBack(obstacleToSpeed(mesures[5]), 1);
        }
      }
      // derrière libre
      else if (!obstacleDetected(6) && mesures[5] >= mesures[4]){
        backward = 1;
        while (obstacleDetected(4)){
          trFrontBack(obstacleToSpeed(mesures[4]), -1);
        }
      }
      // devant + droite + derrière : bloqué
      // gauche libre
      else if (obstacleDetected(0) && obstacleDetected(1) && obstacleDetected(4) && obstacleDetected(5) && obstacleDetected(6)){
        backward = 1;
        while(obstacleDetected(0) || obstacleDetected(1) || obstacleDetected(4) || obstacleDetected(5) || obstacleDetected(6)){
          trLeftRight(obstacleToSpeed(closestObstacle()), 1);
        }
      }
      // Tout autre cas
      else {
        fatalError = 1;
        arret = 1;
        stopMoving();
      }
    }
    // obstacle devant gauche ou obstacle gauche avant
    else if (obstacleDetected(0) || obstacleDetected(2)){
      obstacle = true;
      if (!obstacleDetected(1) && !obstacleDetected(4)){
        while (obstacleDetected(0) || obstacleDetected(2)){
          trDiagonal(obstacleToSpeed(closestObstacle()), 1, 0);
        }
      }
      else {
        stopMoving();
        motion = 0;
        arret = 1;
      }
    }
    // obstacle devant droite ou obstacle droite avant
    else if (obstacleDetected(1) || obstacleDetected(4)){
      obstacle = true;
      if (!obstacleDetected(0) && !obstacleDetected(2)){
        while (obstacleDetected(1) || obstacleDetected(4)){
          trDiagonal(obstacleToSpeed(closestObstacle()), 1, 1);
        }
      }
      else {
        motion = 0;
        arret = 0;
        stopMoving();
      }
    }
    // obstacle derrière
    else if (backward && obstacleDetected(6)){
      obstacle = true;
      if (!obstacleDetected(2) && !obstacleDetected(3)){
        while (!obstacleDetected(5)){
          trLeftRight(obstacleToSpeed(closestObstacle()), 1);
        }
      }
      else if (!obstacleDetected(4) && !obstacleDetected(5)){
        while (!obstacleDetected(3)){
          trLeftRight(obstacleToSpeed(closestObstacle()), -1);
        }
      }
      else {
        motion = 0;
        arret = 0;
        stopMoving();
      }
    }
    // obstacle gauche arriere
    else if (backward && obstacleDetected(3)){
      obstacle = true;
      if (!obstacleDetected(5) && !obstacleDetected(6)){
        while (obstacleDetected(3)){
          trDiagonal(obstacleToSpeed(closestObstacle()), -1, 1);
        }
      }
      else {
        stopMoving();
        motion = 0;
        arret = 1;
      }
    }
    // obstacle droit arrière
    else if (backward && obstacleDetected(5)){
      obstacle = true;
      if (!obstacleDetected(3) && !obstacleDetected(6)){
        while (obstacleDetected(5)){
          trDiagonal(obstacleToSpeed(closestObstacle()), -1, 0);
        }
      }
      else {
        motion = 0;
        arret = 0;
        stopMoving();
      }
    }
  }
  else {
    bool thereIsObstacle = false;
    for (int i=0; (i<(NB_IR_SENSORS+NB_US_SENSORS) && !thereIsObstacle); i++){
      if (obstacleDetected(i)){
        thereIsObstacle = true;
      }
    }
    if (!thereIsObstacle){
      obstacle = false;
      return;
    }
  }
}
