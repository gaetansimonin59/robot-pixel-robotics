#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Stratégie : on quadrille la pièce pour scanner à intervalles réguliers et on combine à la stratégie 2
"""

from math import *
import turtle

turtle.speed(speed=0)

RAYON = 3.5 #portée du capteur
DISTANCE = 2.5 #distance entre les points et les angles
FUSION = 2 #distance minimale entre 2 points

#================================FONCTIONS======================================

def calculScanPieceEntiere(longueurs, angles): #calcule pour une pièce entière les scans nécessaires pour avoir tous les murs
    coordonnees = []

    #met un point en face de chaque angle
    coordonneestmp = []
    x0 = 0
    y0 = 0
    angleTotal = 0
    for i in range(len(longueurs)):
        x0 += longueurs[i]*cos(angleTotal)
        y0 += longueurs[i]*sin(angleTotal)
        xi = -DISTANCE*cos((pi-angles[i])/2)
        yi = DISTANCE*sin((pi-angles[i])/2)
        coordonneestmp.append((xi * cos(angleTotal) + yi * sin(-angleTotal) + x0, yi * cos(angleTotal) + xi * sin(angleTotal) + y0))
        angleTotal += angles[i]

    #calcule les coordonnées des sommmets
    sommets = []
    for j in range(len(longueurs)):
        Xorigine = 0
        Yorigine = 0
        angleTotal = 0
        for l in range(j):
            if l > 0:
                angleTotal += angles[l-1]
            Xorigine += longueurs[l]*cos(angleTotal)
            Yorigine += longueurs[l]*sin(angleTotal)
        sommets.append((Xorigine, Yorigine))
    #calcule les extrémités de la pièce
    xmin = 1000
    ymin = 1000
    xmax = -1000
    ymax = -1000
    for (x, y) in sommets:
        if x > xmax:
            xmax = round(x)
        if x < xmin:
            xmin = round(x)
        if y > ymax:
            ymax = round(y)
        if y < ymin:
            ymin = round(y)

    for i in range(1, 10):
        if 2.5 < (xmax-xmin)/i < 3.5:
            xOFFSET = (xmax-xmin)/i
        if 2.5 < (ymax-ymin)/i < 3.5:
            yOFFSET = (ymax-ymin)/i

    #création des points dans l'espace de la pièce
    x = xmin + xOFFSET
    out = False
    baseOut = False
    while x < xmax:
        y = ymin
        while y < ymax:
            mur = False
            for k in range(len(longueurs)-1):
                if (sommets[k][0] >= x >= sommets[k+1][0]) or (sommets[k][0] <= x <= sommets[k+1][0]):
                    if sommets[k][0]-pow(10, -10) <= sommets[k+1][0] <= sommets[k][0]+pow(10, -10): #le point est sur un mur vertical
                        out = True
                    else:
                        if sommets[k][1]-pow(10, -10) <= sommets[k+1][1] <= sommets[k][1]+pow(10, -10): #on compare à un mur horizontal
                            a = 0
                            b = sommets[k][1]
                        else:
                            a = (sommets[k][1] - sommets[k+1][1]) / (sommets[k][0] - sommets[k+1][0])
                            b = sommets[k][1] - a * sommets[k][0]
                        if (a*x+b) - pow(10, -5) < (y+yOFFSET) < (a*x+b):
                            mur = True
                        elif ((y - (a*x+b)) * ((y+yOFFSET) - (a*x+b)) < 0 or (y+yOFFSET) - (a*x+b) == 0) and out == False:
                            out = True
                        elif (y - (a*x+b)) * ((y+yOFFSET) - (a*x+b)) < 0 and out == True:
                            out = False
            if out is False and mur is False:
                coordonneestmp.append((x, y+yOFFSET))
            y += yOFFSET
        #passage au x suivant
        y = ymin + yOFFSET
        for k in range(len(longueurs)-1):
            if (sommets[k][1] >= y >= sommets[k+1][1]) or (sommets[k][1] <= y <= sommets[k+1][1]):
                if sommets[k][1]-pow(10, -10) <= sommets[k+1][1] <= sommets[k][1]+pow(10, -10): #le point est sur un mur horizontal
                    baseOut = True
                else:
                    if sommets[k][0]-pow(10, -5) <= sommets[k+1][0] <= sommets[k][0]+pow(10, -5): #on compare à un mur vertical
                        if ((x - sommets[k][0]) * ((x+xOFFSET) - sommets[k][0]) < 0 or ((x+xOFFSET) - sommets[k][0]) == 0) and baseOut == False:
                            baseOut = True
                        elif (x - sommets[k][0]) * ((x+xOFFSET) - sommets[k][0]) < 0 and baseOut == True:
                            baseOut = False
                    else:
                        a = (sommets[k][1] - sommets[k+1][1]) / (sommets[k][0] - sommets[k+1][0])
                        b = sommets[k][1] - a * sommets[k][0]
                        if ((x - (y-b)/a) * ((x+xOFFSET) - (y-b)/a) < 0 or ((x+xOFFSET) - (y-b)/a) == 0) and baseOut == False:
                            baseOut = True
                        elif (x - (y-b)/a) * ((x+xOFFSET) - (y-b)/a) < 0 and baseOut == True:
                            baseOut = False
        if baseOut == False:
            coordonneestmp.append((x+xOFFSET, y))
        x += xOFFSET
        out = baseOut

    fusionning = True
    #fusion des points trop proches
    while fusionning is True:
        coordonnees = []
        pointsRemplaces = []
        for j in range(len(coordonneestmp)):
            for k in range(j, len(coordonneestmp)):
                if coordonneestmp[j] != coordonneestmp[k] and coordonneestmp[j] not in pointsRemplaces and coordonneestmp[k] not in pointsRemplaces:
                    if pow(coordonneestmp[j][0]-coordonneestmp[k][0], 2) + pow(coordonneestmp[j][1]-coordonneestmp[k][1], 2) < pow(FUSION, 2):
                        coordonnees.append(((coordonneestmp[j][0]+coordonneestmp[k][0])/2, (coordonneestmp[j][1]+coordonneestmp[k][1])/2))
                        pointsRemplaces.append(coordonneestmp[j])
                        pointsRemplaces.append(coordonneestmp[k])
                    else:
                        if coordonneestmp[j] not in coordonnees:
                            coordonnees.append(coordonneestmp[j])
                        if coordonneestmp[k] not in coordonnees:
                            coordonnees.append(coordonneestmp[k])
        if len(pointsRemplaces) == 0:
            fusionning = False
        else:
            for point in pointsRemplaces:
                if point in coordonnees:
                    coordonnees.remove(point)
        coordonneestmp = coordonnees[:]

    return coordonnees

def dessinScans(coordonnees): #dessine les scans calculés sur le graphique
    for xy in coordonnees:
           turtle.up()
           turtle.goto(30*xy[0], 30*xy[1])
           turtle.dot()
           turtle.setheading(0)
           turtle.forward(30*RAYON)
           turtle.setheading(90)
           turtle.down()
           turtle.circle(30*RAYON)
           turtle.up()
    turtle.down()
    turtle.ht()

#===============================PROGRAMME======================================

#valeurs test de dimensions d'une pièce
"""
longueurs = [7, 4, 2, 4, 6, sqrt(18), 5]
angles = [90, -90, 90, 90, 45, 45, 90]

longueurs = [12, sqrt(160), 8, 12]
angles = [degrees((pi)-atan(12/4)), degrees((pi/2)-atan(4/12)), 90, 90]

longueurs = [10, 8, 2, 4, 6, 4, 2, 8]
angles = [90, 90, 90, -90, -90, 90, 90, 90]
"""
longueurs = [10, 10, 10, 10]
angles = [90, 90, 90, 90]

#dessin de la pièce
for i in range(len(longueurs)):
    turtle.forward(30*longueurs[i])
    turtle.left(angles[i])

#conversion des degrés en radians pour le module math
for angleEnDegres in angles:
    angles[angles.index(angleEnDegres)] = radians(angleEnDegres)

dessinScans(calculScanPieceEntiere(longueurs, angles))

turtle.done()
