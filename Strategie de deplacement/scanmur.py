# -*- coding: utf-8 -*-
"""
Permet de calculer pour un mur droit d'une longueur donnée combien il faut de scans et à quelle distance pour l'avoir en entier.
Stratégie : calculer pour chaque mur le nombre de scans dont on a besoin pour l'avoir en entier,
en prenant en compte le dernier scan du mur précédent (et pour le dernier mur on prend aussi en compte le tout premier scan)
Problème : inadapté pour pièce comportant des "recoins"

Annulé et remplacé par scanPiece.py
"""
from math import *
import turtle

turtle.speed(speed=0)

RAYON = 4 #portée du capteur

def calculScan(l): #calcule pour un mur le nombre de scans nécessaires et leur distance au mur pour l'avoir en entier
    if l <= 0:
        return(0, -1)
    i = 1
    while True:
        if (l/(2*i) < RAYON) and (sqrt(pow(RAYON, 2)-pow(l/(2*i),2)) > 2):
            return (i, sqrt(pow(RAYON, 2)-pow(l/(2*i),2)))
        else:
            i = i + 1

def calculScanPieceEntiere(longueurs, angles): #calcule pour une pièce entière les scans nécessaires pour avoir tous les murs
    nbscans = []
    distances = []
    ecarts = []
    decalages = []

    for i in range(len(longueurs)):
        nbscans.append(0)
        distances.append(0)
        decalages.append(0)
        ecarts.append(0)
        if i == 0 and angles[i] > 0:
            nbscans[i] += calculScan(longueurs[i])[0]
            distances[i] += calculScan(longueurs[i])[1]
            ecarts[i] += (longueurs[i]) / (2*nbscans[i])
            continue
        elif i == 0 and angles[i] < 0:
            nbscans[i] += calculScan(2*longueurs[i])[0]
            distances[i] += calculScan(2*longueurs[i])[1]
            ecarts[i] += (2*longueurs[i]) / (2*nbscans[i])
            continue
        if i == len(longueurs) - 1:
            if ecarts[i-1] != 0:
                dec = 2*RAYON*cos(pi-angles[i-1]-acos(ecarts[i-1]/RAYON)) #distance du mur déjà scannée
            else:
                dec = 0
            if dec > 0:
                decalages[i] += dec
            dec2 = 2*RAYON*cos(pi-angles[i-1]-acos(ecarts[i-1]/RAYON)) #distance du mur déjà scannée
            if dec2 > 0:
                decalages.append(dec2)
            nbscans[i] += calculScan(longueurs[i]-(decalages[-1])-(decalages[-2]))[0]
            distances[i] += calculScan(longueurs[i]-(decalages[-1])-(decalages[-2]))[1]
            if nbscans[i] != 0:
                ecarts[i] += (longueurs[i]-(decalages[-1])-(decalages[-2]))/(2*nbscans[i])
            continue
        if angles[i-1] > 0:
            if ecarts[i-1] != 0:
                dec = 2*RAYON*cos(pi-angles[i-1]-acos(ecarts[i-1]/RAYON)) #distance du mur déjà scannée
            else:
                dec = 0
            if dec > 0:
                decalages[i] += dec
        else:
            #CALCULER DECALAGE DU AU DEPASSEMENT
            pass
        if angles[i] > 0:
            nbscans[i] += calculScan(longueurs[i]-(decalages[i]))[0]
            distances[i] += calculScan(longueurs[i]-(decalages[i]))[1]
            if nbscans[i] != 0:
                ecarts[i] += (longueurs[i]-(decalages[i]))/(2*nbscans[i])
        else:
            nbscans[i] += calculScan((longueurs[i]-(decalages[i])))[0]
            distances[i] += (calculScan((longueurs[i]-(decalages[i])))[1])
            if nbscans[i] != 0:
                ecarts[i] += ((longueurs[i]-(decalages[i])))/(2*nbscans[i])


    #ESSAYER DE MIEUX GERER LES ANGLES NEGATIFS ET D'ELOIGNER LES SCANS TROP PRES DES MURS
    """
    for i in range(len(longueurs)):
        if (i == 0 or angles[i-1] < 0) and i != len(longueurs)-1: #premier mur et ceux sans décalage car angle négatif
            nbscans.append(calculScan(longueurs[i])[0])
            distances.append(calculScan(longueurs[i])[1])
            decalages.append(0)
            if nbscans[i] != 0:
                ecarts.append((longueurs[i]-(decalages[-1]))/(2*nbscans[i]))
            else:
                ecarts.append(-1)
        elif i < len(longueurs)-1:
            dec = 2*RAYON*cos(pi-angles[i-1]-acos(ecarts[-1]/RAYON)) #distance du mur déjà scannée
            if dec > 0:
                decalages.append(dec)
            else:
                decalages.append(0)
            nbscans.append(calculScan(longueurs[i]-(decalages[-1]))[0])
            distances.append(calculScan(longueurs[i]-(decalages[-1]))[1])
            if nbscans[i] != 0:
                ecarts.append((longueurs[i]-(decalages[-1]))/(2*nbscans[i]))
            else:
                ecarts.append(-1)
        else: #dernier mur
            dec = 2*RAYON*cos(pi-angles[i-1]-acos(ecarts[-1]/RAYON)) #distance du mur déjà scannée
            if dec > 0:
                decalages.append(dec)
            else:
                decalages.append(0)
            dec2 = 2*RAYON*cos(pi-angles[-1]-acos(ecarts[0]/RAYON))
            if dec2 > 0:
                decalages.append(dec2)
            else:
                decalages.append(0)
            nbscans.append(calculScan(longueurs[i]-(decalages[-1])-(decalages[-2]))[0])
            distances.append(calculScan(longueurs[i]-(decalages[-1])-(decalages[-2]))[1])
            if nbscans[i] != 0:
                ecarts.append((longueurs[i]-(decalages[-1])-(decalages[-2]))/(2*nbscans[i]))
            else:
                ecarts.append(-1)
"""
    coordonnees = []
    for j in range(len(nbscans)):
        coordonnees.append([])
        if j == 0:
            for k in range(nbscans[j]):
                coordonnees[j].append(((2*k+1)*ecarts[0], distances[0]))
        else:
            Xorigine = 0
            Yorigine = 0
            angleTotal = 0
            for l in range(j):
                if l > 0:
                    angleTotal += angles[l-1]
                Xorigine += longueurs[l]*cos(angleTotal)
                Yorigine += longueurs[l]*sin(angleTotal)
            for k in range(nbscans[j]):
                co = (decalages[j]+(2*k+1)*ecarts[j], distances[j])  #coordonnées dans le repère du mur j
                coordonnees[j].append((co[0]*cos(angles[j-1]+angleTotal)+co[1]*sin(-(angles[j-1]+angleTotal))+Xorigine, co[0]*sin(angles[j-1]+angleTotal)+co[1]*cos(angles[j-1]+angleTotal)+Yorigine))
    print("nbscans", nbscans, "\ndistances", distances, "\necarts", ecarts, "\ndécalages", decalages)
    return(coordonnees)

def dessinScans(coordonnees): #dessine les scans calculés sur le graphique
    for mur in coordonnees:
        for xy in mur:
            turtle.up()
            turtle.goto(10*xy[0], 10*xy[1])
            turtle.dot()
            turtle.setheading(0)
            turtle.forward(10*RAYON)
            turtle.setheading(90)
            turtle.down()
            turtle.circle(10*RAYON)
            turtle.up()
    turtle.down()
    turtle.ht()

#valeurs test de dimensions d'une pièce

longueurs = [14, 8, 4, 8, 12, 2*sqrt(18), 10]
angles = [90, -90, 90, 90, 45, 45, 90]
"""
longueurs = [12, sqrt(160), 8, 12]
angles = [degrees((pi)-atan(12/4)), degrees((pi/2)-atan(4/12)), 90, 90]

longueurs = [10,8,2,4,6,4,2,8]
angles = [90,90,90,-90,-90,90,90,90]
"""
#dessin de la pièce
for i in range(len(longueurs)):
    turtle.forward(10*longueurs[i])
    turtle.left(angles[i])

#conversion des degrés en radians pour le module math
for angleEnDegres in angles:
    angles[angles.index(angleEnDegres)] = radians(angleEnDegres)

"""
#TEST calculScan()
l = int(input("Longueur du mur : "))
print("Il faudra {} scan(s) à une distance de {} m du mur".format(calculScan(l)[0], calculScan(l)[1]))
"""

dessinScans(calculScanPieceEntiere(longueurs, angles))

turtle.done()
