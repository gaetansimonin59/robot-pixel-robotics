# -*- coding: utf-8 -*-
"""
Permet de calculer pour un mur droit d'une longueur donnée combien il faut de
scans et à quelle distance pour l'avoir en entier
Stratégie : on fait un scan à une certaine distance de chaque angle de la pièce
sur sa bissectrice

Annulé et remplacé par scanPiece.py
"""

from math import *
import turtle

turtle.speed(speed=0)

RAYON = 4 #portée du capteur
DISTANCE = 2.5

#===============================FONCTIONS======================================

def calculScanPieceEntiere(longueurs, angles): #calcule pour une pièce entière les scans nécessaires pour avoir tous les murs
    coordonnees = []
    coordonneestmp = []
    x0 = 0
    y0 = 0
    angleTotal = 0
    for i in range(len(longueurs)):
        x0 += longueurs[i]*cos(angleTotal)
        y0 += longueurs[i]*sin(angleTotal)
        xi = -DISTANCE*cos((pi-angles[i])/2)
        yi = DISTANCE*sin((pi-angles[i])/2)
        coordonneestmp.append((xi * cos(angleTotal) + yi * sin(-angleTotal) + x0, yi * cos(angleTotal) + xi * sin(angleTotal) + y0))
        angleTotal += angles[i]

    for i in range(4):
        coordonnees = []
        pointsRemplaces = []
        for j in range(len(coordonneestmp)):
            for k in range(j, len(coordonneestmp)):
                if coordonneestmp[j] != coordonneestmp[k]:
                    if pow(coordonneestmp[j][0]-coordonneestmp[k][0], 2) + pow(coordonneestmp[j][1]-coordonneestmp[k][1], 2) < pow(2, 2):
                        coordonnees.append(((coordonneestmp[j][0]+coordonneestmp[k][0])/2, (coordonneestmp[j][1]+coordonneestmp[k][1])/2))
                        pointsRemplaces.append(coordonneestmp[j])
                        pointsRemplaces.append(coordonneestmp[k])
                    else:
                        if coordonneestmp[j] not in coordonnees:
                            coordonnees.append(coordonneestmp[j])
                        if coordonneestmp[k] not in coordonnees:
                            coordonnees.append(coordonneestmp[k])
        for point in pointsRemplaces:
            if point in coordonnees:
                coordonnees.remove(point)
        coordonneestmp = coordonnees[:]
    print(coordonnees)
    return coordonnees

def dessinScans(coordonnees): #dessine les scans calculés sur le graphique
    for xy in coordonnees:
           turtle.up()
           turtle.goto(30*xy[0], 30*xy[1])
           turtle.dot()
           turtle.setheading(0)
           turtle.forward(30*RAYON)
           turtle.setheading(90)
           turtle.down()
           turtle.circle(30*RAYON)
           turtle.up()
    turtle.down()
    turtle.ht()

#===============================PROGRAMME======================================

#valeurs test de dimensions d'une pièce

longueurs = [7, 4, 2, 4, 6, sqrt(18), 5]
angles = [90, -90, 90, 90, 45, 45, 90]
"""
longueurs = [12, sqrt(160), 8, 12]
angles = [degrees((pi)-atan(12/4)), degrees((pi/2)-atan(4/12)), 90, 90]

longueurs = [10,8,2,4,6,4,2,8]
angles = [90,90,90,-90,-90,90,90,90]
"""
#dessin de la pièce
for i in range(len(longueurs)):
    turtle.forward(30*longueurs[i])
    turtle.left(angles[i])

#conversion des degrés en radians pour le module math
for angleEnDegres in angles:
    angles[angles.index(angleEnDegres)] = radians(angleEnDegres)

"""
#TEST calculScan()
l = int(input("Longueur du mur : "))
print("Il faudra {} scan(s) à une distance de {} m du mur".format(calculScan(l)[0], calculScan(l)[1]))
"""

dessinScans(calculScanPieceEntiere(longueurs, angles))

turtle.done()
