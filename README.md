# Projet Pixel Robotics | Polytech Lille

Période de conception : du 31-06-2021 au 09-07-2021

Collaborateurs :  
\- Titouan Azimzadeh  
\- Gaëtan Simonin

Ce dépôt `GitLab` contient le projet `Arduino` sur lequel nous avons travaillé durant notre 8e semestre d'études supérieures dans le cadre de notre formation d'ingénieur en Système Embarqué (anciennement IMA) à Polytech' Lille. Il s'inscrit dans un projet en partenariat avec l'association `Alors On Bouge` pour le compte de l'entreprise `Pixel Robotics`. N'hésitez pas à cloner ce dépôt et l'adapter à vos besoins.

## Présentation du projet

L'entreprise `Pixel Robotics` a pour but de faire découvrir et démocratiser la modélisation 3D au grand public. Pour scanner des pièces de grande tailles (environ 100 m²), l'entreprise a besoin de développer un robot capable de se déplacer en autonomie équipé d'un capteur 3D de leur confection. 

## Contenu du dépôt
- `automate/` : le dossier contient toutes les fonctions de déplacement du robot en autonomie.  
- `cmdManuelle/` : le dossier contenant toutes les fonctions de déplacement du robot en mode manuel.
- `Strategie de deplacement/` : le dossier contient toutes les fonctions pour déterminer les points où le robot doit se rendre.
- `tests/` : le dossier contient tous les fichiers de test (se référer au fichier readme de ce dossier).

## Améliorations proposées :
- Electronique de puissance :
	- Trouver et résoudre le soucis de la carte de puissance (pb au niveau de la conversion de tension).  
	- Relier tous les composants et ce de façon propre (cable management).    
   
- Mécanique :
	- Améliorer le chassis du robot pour qu'il soit plus robuste (surtout les accouplements).  
	- Continuer de développer des roues dentées pour les capteurs de vitesse de chaque moteur.    
  
- Algorithmes :
	- Intégrer correctement les moteurs dans le script cmdManuelle pour que les deplacements diagonaux soient corrects.   
	- Fusionner cmdManuelle et automate afin d'avoir un seul dossier permettant de passer du mode manuel au mode autonome facilement.  
	- Terminer le fichier automate.ino, pour que le robot finisse sa tache de cartographie en autonomie.  
	- Améliorer l'application Bluetooth pour qu'elle soit parfaitement opérationnelle et en accord avec la charte graphique de l'entreprise.  
	- Continuer les tests sur les capteurs ainsi que leur calibrage.  
