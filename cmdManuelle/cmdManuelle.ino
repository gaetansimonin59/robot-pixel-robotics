/*
      cmdManuelle.ino File : Contains setup and loop functions and all variables.

 Copyright (c) | 2021 | Pixel Robotics' project. All rights Reserved.
 Contributors : Titouan Azimzazdeh et Gaëtan Simonin.
 School : Polytech Lille.
 */

#include <SoftwareSerial.h>
#include <math.h>

SoftwareSerial BTSerial(11,10); // RX | TX
String msg = "-";
int wheelSpeed = 0;
/* ------------Définition des constantes------------- */
#define MEASURE_TIMEOUT       3500UL                // timeout constant : 25ms = ~8m à 340m/s
#define SOUND_SPEED           340.0/1000             // Sound speed in the air [mm/us]
#define NB_MESURES            30                     // number of measures for an average distance
#define NB_US_SENSORS         4                      // number of ultrasonic sensors
#define NB_IR_SENSORS         7                      // number of infrared sensors
#define NB_MOTORS             4                      // Number of engines
#define PWM_RATIO             206                    // because in the formula : Vmax * 256/Vmax
#define NB_MAX_BAR            20                     // Maximum bars on the speed sensor wheel
#define ROBOT_DIAMETER        459.324
#define WHEEL_DIAMETER        127                    // Length of the omnidirectional wheel in mm
#define DISTANCE_TRAVELED(m)  PI*WHEEL_DIAMETER*barCounter[m]/NB_MAX_BAR
#define DEGRE_TO_RADIAN(a)    a*PI/180
#define RADIAN_TO_DEGRE(a)    a*180/PI
#define txPin                 18
#define rxPin                 19
#define BAUDRATE              9600
#define DISTANCE_BETWEEN_SIDE_SENSORS 135
#define DISTANCE_BETWEEN_FRONT_SENSORS 100

/* ------------------pins------------------ */
const byte US_SENSORS_PINS[NB_US_SENSORS][2] = {{30, 31}, {32, 33}, {34, 35}, {36, 37}};       // Array of Trigger and Echo pins of each ultrasonic distance sensor [trigger, echo]
const byte SPEED_SENSORS_PINS[NB_MOTORS] = {50, 51};                                   // Array of the pin map of every speed sensor : 50 front right, 51 rear right, 52 front left, 53 rear left
const byte IR_SENSORS_PINS[NB_IR_SENSORS] = {0, 1, 2, 3, 4, 5, 6};                             // Array of analog pin of each infrared sensor : 0 avant gauche, 1 avant droit, 2 gauche devant, 3 gauche derriere, 4 droit devant, 5 droite derriere, 6 arriere
const byte MOTORS_PINS[NB_MOTORS][3] = { {24, 25, 3},{23, 22, 2}, {26, 27, 4}, {29, 28, 5}};  // [[Pin1, Pin2, SpeedPin], ...]

float mesures[NB_IR_SENSORS + NB_US_SENSORS] = {0};  // last measure of each sensor

int etape = 0;    //  0 : recherche de mur
                  //  1 : placement parallèle au mur
                  //  2 : recherche d'un angle
                  //  3 : mesure du mur
bool arret = false;                        // set to 1 when the robot is stopped
int capteur_arret;
int action;
int capteurs_mur[2];
float mesures_mur[2];
int sense_of_rotation;
int barCounter[4] = {0, 0, 0, 0};     // Counter of the number of bar crossed
int oldBarCounter[4] = {0};           // Save the old values of barCounter
int motion = 0, fatalError = 0;
float x = 0., y = 0., angle = 0.;     // robot coordinates
bool testingEndOfWall = false;
int mur = -1;
int origines[30][2];
int tmpOrigin[3] = {0, 0, 0};
bool obstacle = false, backward = false;
int obDetect[NB_IR_SENSORS + NB_US_SENSORS] = {0};
int currentCmd = 0;
float xRef = x, yRef = y, angleRef = angle;

bool automatic = true;

typedef struct {
  int intensity;  // in percentage
  float angle;  // in degre
} Vector;

void resetSpeedSensors(){
  barCounter[0] = 0;
  barCounter[1] = 0;
  barCounter[2] = 0;
  barCounter[3] = 0;
}

void Scan(){
}

void setup(){
  pinMode (11, INPUT);
  pinMode (10, OUTPUT);

  BTSerial.begin(9600);
  Serial.begin(9600);

  for (int i=0; i<NB_MOTORS; i++){
    pinMode(MOTORS_PINS[i][0], OUTPUT);
    pinMode(MOTORS_PINS[i][1], OUTPUT);
    pinMode(MOTORS_PINS[i][2], OUTPUT);
  }
}

void loop(){
        /*aquisitionIR(mesures, 0, NB_IR_SENSORS-1);
        aquisitionUS(&(mesures[NB_IR_SENSORS]));
        for (int i=0; (i<= (NB_IR_SENSORS+NB_US_SENSORS) && obstacle == false); i++){
          if (mesures[i] < 250) obstacle = true;
        }*/
        //if (obstacle == false){
          readBT();
          convertionCmdBT();
        //}
}
