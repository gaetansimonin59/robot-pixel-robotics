/*
      bluetooth.ino File : Contains all functions for Bluetooth communication.

 Copyright (c) | 2021 | Pixel Robotics' project. All rights Reserved.
 Contributors : Titouan Azimzazdeh et Gaëtan Simonin.
 School : Polytech Lille.
 */


void readBT(){
	// Fonction d'ecoute du Bluetooth
  msg = "";
  while (BTSerial.available()) {
    // on a au moins 1 octet en attente
    int c = BTSerial.read(); // on lit la valeur. la fonction read retourne un entier: -1 en cas d'erreur sinon l'octet lu (dans l'octet de poids faible de l'entier)
    if (c != -1) { // s'il n'y a pas eu d'erreur de lecture
      switch (c) {
        case '\n':
          Serial.print(F("message complet : ")); Serial.println(msg);
          return;
        case '\r':
          break;
        default:
          msg += (char) c;
		// Serial.print(F(" caractere: [")); Serial.print((char) c); Serial.println(F("]"));
          break;
      }
    }
  }
  msg = "-";
}

void convertionCmdBT(){
	// Fonction de conversion de ce qui a ete lu sur le Bluetooth en ordre de déplacement
  if(!msg.equals("-")){
	int cmd = msg.toInt();
	// Mouvement souhaité
	if (cmd >= -5 && cmd <= 5){
            if (wheelSpeed == 0) wheelSpeed = 122; // defaultSpeed
		switch (cmd){
			case 0:
				stopMoving();
				break;
			case 1:
				trFrontBack(wheelSpeed, cmd);
				break;
			case -1:
				trFrontBack(wheelSpeed, cmd);
				break;
			case 2:
				trLeftRight(wheelSpeed, 1);
				break;
			case -2:
				trLeftRight(wheelSpeed, -1);
				break;
			case 3:
				trDiagonal(wheelSpeed, 1, 1);
				break;
			case -3:
				trDiagonal(wheelSpeed, -1, 1);
				break;
			case 4:
				trDiagonal(wheelSpeed, 1, 0);
				break;
			case -4:
				trDiagonal(wheelSpeed, -1, 0);
				break;
			case 5:
				rotation(wheelSpeed, -1);
				break;
			case -5:
				rotation(wheelSpeed, 1);
				break;
		}
                currentCmd = cmd;
	}
	// potar de vitesse
	else if (cmd >= 16 && cmd <= 255){
                 wheelSpeed = floor(0.418*cmd-6.69+0.5);
		//wheelSpeed = floor(0.65*cmd+89.6 + 0.5);
                //wheelSpeed = floor(0.774*cmd+57.616 +0.5);
                Serial.println(wheelSpeed);
                msg = String(currentCmd);
                convertionCmdBT();
	}
	// Bouton reset enclenché
	else if (msg == "reset"){
		wheelSpeed = 0;
		x = 0.; y = 0.; angle = 0.;
		stopMoving();
	}
	// Bouton run enclenché
	else if (msg == "run"){
                msg = String(currentCmd);
		convertionCmdBT();
	}
	// Bouton pause enclenché
	else if (msg == "pause"){
		stopMoving();
	}
        // Bouton scan enclenché
	else if (msg == "scan"){
		Scan();
	}
        else if (msg == "manuel"){
                automatic = false;
                Serial.println("girafe");
        }
        else if (msg == "auto"){
                automatic = true;
                Serial.println("blable");
        }

        msg = "-";
  }
}
